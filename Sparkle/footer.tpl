<if isset($userinfo['loginname'])>
	</div>
	<div class="clear"></div> 
	</div>
</if>
<footer>
	<span><img src="templates/{$theme}/assets/img/logo_grey.png" alt="Froxlor" /> 
		<if (Settings::Get('admin.show_version_login') == '1' && $filename == 'index.php') || ($filename != 'index.php' && Settings::Get('admin.show_version_footer') == '1')>
			{$version}{$branding}
		</if>
	      Copyleft-{$current_year}  <a href="http://goalhost.net" rel="external">Goalhost</a><br />
	</span>
	<if $lng['translator'] != ''>
		<br /><span>{$lng['panel']['translator']}: {$lng['translator']}
	</if>
</footer>
</body>
</html>
